/*********************************************
 * OPL 12.7.1.0 Model
 * Author: jmoll
 * Creation Date: Nov 16, 2017 at 10:41:13 AM
 *********************************************/


// ========= DATA TYPES =============
// Data type to hold information about each edge
tuple Edge {
   key int FromNode;   // Source Node
   key int ToNode;     // Destination Node
}

// ============ DATA ===============
// Graph Data
int NumberOfNodes = ...;           // Number of nodes on the network (with the Sink)
range Nodes = 1..NumberOfNodes;    // Set of Nodes
int SinkNode = ...;                // Sink Node number
int InitialPayloads[Nodes] = ...;  // Initial number of payloads at each node
{Edge} Edges = ...;                // Set of edges of the network
int DistanceToSink[Nodes] = ...;   // Distance to the Sink (0 is Sink)

// Time Data
int TimeFrame = ...;                          // Global TimeFrame
range TimeSlots = 1..TimeFrame;               // set of TimeSlots
int alpha = ...;                              // Periods duration (see beta)
float beta = ...;                             // Minimum Payloads Per Node that sink should receive in a certain period
range TimePeriods = 1..(TimeFrame div alpha); // set of TimePeriods

// Nodes States
{string} StateNames = ...;         // Names of the states

// Size Constraints Data
int MaxPayloadsPerMsg = ...;       // Max Payloads per message package 
int BufferSize[Nodes] = ...;       // Maximum number of Payloads that a Node can contain

// Battery Data
float BMin[Nodes] = ...;               // Minimum Battery 
float BMax[Nodes] = ...;               // Maximum Battery
float InitialBattery[Nodes] = ...;             // Initial Battery

// Energy Data
float EnergyHarvest[TimeSlots] = ...;  // Increase of Battery
float EnergyCost[StateNames] = ...;    // Decrease of Battery

// Events
int EventsStartingNearby[Nodes][TimeSlots] = ...;   // Nodes that can capture an event nearby
int EventsStartingNow[TimeSlots] = ...;             // 1- Events starts/exists on that TimeSlot; 0- otherwise
int EventsDuration = ...;                           // Fixed Event Duration, in TimeSlots

// Other
int MinTxPerPeriod = 1;
int MinSensingPerPeriod = 1;

// =========== DECISION VARS ==============
// Amount of Payloads in the Edge between two Nodes
dvar int+ PayloadsInEdge[e in Edges][t in TimeSlots];

// Amount of Headers in the Flow between two Nodes
dvar int+ HeadersInEdge[e in Edges][t in TimeSlots];

// Boolean var that shows if Node i at time t is at state m
dvar boolean NodeStates[i in Nodes][t in TimeSlots][m in StateNames];

// Boolean var that shows if there's any payload at Edge e at time t
dvar boolean EdgeHasPayloads[e in Edges][t in TimeSlots]; 

// Boolean var that shows if a Message at Edge e at time t is not complete
dvar boolean MsgIsNotComplete[e in Edges][t in TimeSlots];

// Amount of Battery at Node i at time t
dvar float+ Battery[i in Nodes][t in TimeSlots];

// Amount of Payloads at Node i at time t
dvar int+ Payloads[i in Nodes][t in TimeSlots];

// Total payloads that arrive at Sink Node
//dexpr int PayloadsAtSinkNode =
//			sum(<i,SinkNode> in Edges, t in TimeSlots) PayloadsInEdge[<i,SinkNode>][t];

// The more a Payload stays away from the Sink, the more "Weight"" it has
dexpr int Weight[t in TimeSlots] = sum(i in Nodes) Payloads[i][t] * DistanceToSink[i];

// ============ EXECUTION SETTINGS ==============         
execute RunSettings
{
  //cplex.intsollim = 2;     // Faster results with a limit on the maximum solutions number
  cplex.simdisplay = 1;      // No iteration messages until solution
  cplex.mipdisplay = 1;      // Display integer feasible solutions
  cplex.mipemphasis = 2;     // Emphasize optimality over feasibility 
  cplex.conflictdisplay = 2; // Detailed conflict information display
  //cplex.tilim = 60*60;       // Maximum time, in seconds, for a call to an optimizer
  cplex.solnpoolcapacity = 0; // No Solution Pool
}

// ============= OBJECTIVE =================
// Maximizing total payloads that arrive at Sink Node over time
maximize Payloads[SinkNode][TimeFrame] - 0.5*(Weight[5] + Weight[10] + Weight[15]+Weight[TimeFrame]);

// ============= CONSTRAINTS =================
subject to
{
	forall (t in TimeSlots)
	{
	    // Sink Node is always in Rx Mode (v)
	    ctSinkIsInRx:
			NodeStates[SinkNode][t]["Rx"] == 1;
			
		// At least one node must capture an nearby event in the first half of its duration (v)
		ctEvent:
			sum(i in Nodes, l in 0..(EventsDuration div 2): (t+l) <= TimeFrame)
			  NodeStates[i][t+l]["sensing"] * EventsStartingNearby[i][t] >= EventsStartingNow[t];
	}
	
	forall (t in TimeSlots: t != TimeFrame)
	{
	  	ctSinkFlow:
			Payloads[SinkNode][t+1] == Payloads[SinkNode][t]                            
			+ sum(<i,SinkNode> in Edges) PayloadsInEdge[<i,SinkNode>][t+1];    
	}
    
	ctSinkFlowStart:
			Payloads[SinkNode][1] == InitialPayloads[SinkNode]                         
			+ sum(<i,SinkNode> in Edges) PayloadsInEdge[<i,SinkNode>][1]; 
				
	forall (i in Nodes: i != SinkNode, t in TimeSlots)
	{
		// A node can only be in one state at time t	(v)
		ctOnlyOneState:
			sum(state in StateNames) NodeStates[i][t][state] == 1;
		
		
	   // Node Buffer Max Capacity (v)
		 ctMaxBuffer:
	    	Payloads[i][t]            // Number of Payloads at any Node                                          
			<=                        // need to be less or equal than
			BufferSize[i];            // the Node Buffer Size
		
		// Asserts that a node is in "Tx" state only if transmitting data (v)
		ctTxOnlyWhenTransmitting:	
			NodeStates[i][t]["Tx"] <= sum(<i,j> in Edges) EdgeHasPayloads[<i,j>][t];
		// Asserts that a node is in "Rx" state only if receiving data (v)
		ctRxOnlyWhenReceiving:
			NodeStates[i][t]["Rx"] <= sum(<j,i> in Edges) EdgeHasPayloads[<j,i>][t];
		
		// Battery Min-Max constraints (v)
		ctBatteryMin:	
			Battery[i][t] >= BMin[i];    
		ctBatteryMax:
			Battery[i][t] <= BMax[i];
   }
   


	forall (period in TimePeriods)
	{
		// Must arrive a certain number of payloads beta(k) during a period of alpha time (v)
		ctAlphaBeta:	
			sum(t in TimeSlots: t > (period-1)*alpha && t <= period*alpha, <j,SinkNode> in Edges)
			  	PayloadsInEdge[<j,SinkNode>][t]
			>=
			(NumberOfNodes-1)*beta;
		
		// A node must transmit something and be in sensing at least a minimum amount per period
//		forall (i in Nodes: i != SinkNode)
//		{
//			ctActivityPerPeriodTx:		
//				sum(t in TimeSlots: t > (period-1)*alpha && t <= period*alpha) NodeStates[i][t]["Tx"]
//				>= MinTxPerPeriod;
//		  
//			ctActivityPerPeriodSensing:  
//				sum(t in TimeSlots: t > (period-1)*alpha && t <= period*alpha) NodeStates[i][t]["sensing"] 
//		    	>= MinSensingPerPeriod;
//  		}		  
	}
	
	

	forall(<i,j> in Edges, t in TimeSlots)
	{   
		// If an edge is active it must have payloads (v)
		ctFlowExistanceMin:
			EdgeHasPayloads[<i,j>][t] <= PayloadsInEdge[<i,j>][t]; 	      
		ctFlowExistanceMax:
	  		maxint*EdgeHasPayloads[<i,j>][t] >= PayloadsInEdge[<i,j>][t];
	  	
	  	// If a certain edge is active then one node is in Tx and the other is in Rx (v)
	  	ctTxRx:
			NodeStates[i][t]["Tx"] + NodeStates[j][t]["Rx"] >= 2*EdgeHasPayloads[<i,j>][t];
		
		// If an edge has payloads, it must have the right amount of Headers in the Message (v)
		ctHeaderCalcMin:	
			PayloadsInEdge[<i,j>][t] <= MaxPayloadsPerMsg * HeadersInEdge[<i,j>][t];
		ctHeaderCalcMax:	
			PayloadsInEdge[<i,j>][t] >= 
				MaxPayloadsPerMsg * (HeadersInEdge[<i,j>][t] - MsgIsNotComplete[<i,j>][t]) + 0.1*MsgIsNotComplete[<i,j>][t];
			
		//MsgIsNotComplete[<i,j>][t] <= EdgeHasPayloads[<i,j>][t];                                            	                                          
 	}	  	
	

	forall (i in Nodes: i!= SinkNode)
	{  
		// Battery level at end of TimeFrame must be higher or equal than at beginning (v)
		ctNeutralOperation:           
			Battery[i][TimeFrame] >= InitialBattery[i]; 
			 
		// Battery level calculation for first TimeSlot (-)
	    ctBatteryCalcStart:
			Battery[i][1] <= InitialBattery[i]                      // Initial Battery level
			+ EnergyHarvest[1]                                      // plus harvest
			- EnergyCost["sensing"] * NodeStates[i][1]["sensing"]   // minus cost if in "Sensing"
			- EnergyCost["sleeping"] * NodeStates[i][1]["sleeping"] // minus cost if in "Sleeping"
			- EnergyCost["Tx"] * NodeStates[i][1]["Tx"]             // minus cost if in "Tx"
			- EnergyCost["Rx"] * NodeStates[i][1]["Rx"];            // minus cost if in "Rx"
			
			
		// Preserve flows at each node in each timeslot: (v)
	   	// they need to arrive more payloads to any Node than those the Node sends
		ctNodeFlowStart:
			Payloads[i][1] == InitialPayloads[i]                // Payloads at Node i, in t+1 are equal at those in the last TimeSlot
			+ NodeStates[i][1]["sensing"]                       // plus the Payloads created in the Node i because of Sensing
			+ sum(<j,i> in Edges) PayloadsInEdge[<j,i>][1]      // plus sum of Payloads entering Node i
			- sum(<i,j> in Edges) PayloadsInEdge[<i,j>][1];     // minus sum of Payloads leaving Node i
	}
		
	forall(i in Nodes: i!= SinkNode, t in TimeSlots: t != TimeFrame)
	{
		// Preserve flows at each node in each timeslot: (v)
	   	// they need to arrive more payloads to any Node than those the Node sends
		ctNodeFlow:
			Payloads[i][t+1] == Payloads[i][t]                    // Payloads at Node i, in t+1 are equal at those in the last TimeSlot
			+ NodeStates[i][t+1]["sensing"]                       // plus the Payloads created in the Node i because of Sensing
			+ sum(<j,i> in Edges) PayloadsInEdge[<j,i>][t+1]      // plus sum of Payloads entering Node i
			- sum(<i,j> in Edges) PayloadsInEdge[<i,j>][t+1];     // minus sum of Payloads leaving Node i
	
		// Same as ctBatteryCalcStart for all remaining TimeSlots (-)
		ctBatteryCalc:
			Battery[i][t+1]	<= Battery[i][t]
			+ EnergyHarvest[t+1]
			- EnergyCost["sensing"] * NodeStates[i][t+1]["sensing"]                     
			- EnergyCost["sleeping"] * NodeStates[i][t+1]["sleeping"]                   
			- EnergyCost["Tx"] * NodeStates[i][t+1]["Tx"]  
			- EnergyCost["Rx"] * NodeStates[i][t+1]["Rx"];  
	}
}



// ============ POST-PROCESSING ===============
execute SaveNodeResults {
	var outfile = new IloOplOutputFile("NodeResults.txt");
		
	// Write Nodes States
	for(var t in thisOplModel.TimeSlots)	
		for(var i in thisOplModel.Nodes) 
			outfile.writeln(t, " | ", i, " | ", thisOplModel.NodeStates[i][t]);
	outfile.writeln("");
	
	outfile.close();
}

execute SaveEdgeResults {
	var outfile = new IloOplOutputFile("EdgeResults.txt");

	// Write flows in edges
	for(var t in thisOplModel.TimeSlots)
		for(var e in thisOplModel.Edges)
			if(thisOplModel.PayloadsInEdge[e][t] > 0)
				outfile.writeln(t, " | ", e.FromNode, " > ", e.ToNode, " | [", thisOplModel.HeadersInEdge[e][t], "] ", thisOplModel.PayloadsInEdge[e][t]);		
						
	outfile.close();			
}

execute SaveBatteryResults {
	var outfile = new IloOplOutputFile("BatteryResults.txt");
	
	// Write battery state for the TimeFrame
	for(var t in thisOplModel.TimeSlots)	
		for(var i in thisOplModel.Nodes) 
			outfile.writeln(t, " | ", i, " | ", thisOplModel.Battery[i][t]);

	outfile.close();		
}
